# Workshop 6: Python Tool at Risø

This is the sixth in a series of scientific Python lectures at Risø campus,
Denmark Technical University.

## Workshop objective

To increase communication amongst researchers here at DTU Wind Energy on which
Python packages are under development and/or should be used in our research.

## Schedule

Please note that this schedule is tentative and therefore subject to change.

- 10:00: Welcome (Jenni Rinker)
- 10:05: [PyConTurb](https://gitlab.windenergy.dtu.dk/rink/pyconturb) (Jenni Rinker)
- 10:20: Volt (Elliot Simon)
- 10:35: Pungi (Neil Davis)
- 10:50: Break
- 11:00: [TOPFARM](https://github.com/TOPFARM-Wind/topfarm) (Jenni Rinker)
- 11:15: [xarray](http://xarray.pydata.org/en/stable/) (Bjarke Tobias Olsen)
- 11:30: [HawtOpt2](https://gitlab.windenergy.dtu.dk/HAWTOpt2) (Mike McWilliam)
- 11:45: [Wind Energy Toolbox](https://gitlab.windenergy.dtu.dk/toolbox/WindEnergyToolbox) (David Verelst)
- 12:00: Lunch

## Who should come

Anyone interested in learning about the various Python-based tools that have
either been made here at Risø or are useful for wind energy researchers.

## Date

The workshop date and location will be announced internally at DTU Risø. Please
use the contact information below for questions on the workshop contents or 
arranging a new workshop.

## Prerequisites

None! :)  

## Contact

Jenni Rinker  
rink@dtu.dk